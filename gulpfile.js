/**
 * -------------------------------------------------------------------------------------------------
 * Gulp Config
 * -------------------------------------------------------------------------------------------------
 */

const   browserSync         = require( 'browser-sync' ),
        changed             = require( 'gulp-changed' ),
        del                 = require( 'del' ),
        gulp                = require( 'gulp' ),
        gulpConfig          = require( './conf/gulp.config.json' )
        gulpTap             = require( 'gulp-tap' ),
        imagemin            = require( 'gulp-imagemin' ),
        path                = require( 'path' ),
        phpLint             = require( 'gulp-phplint' ),
        sass			    = require( 'gulp-sass' ),
        sassJsonImporter    = require( 'node-sass-json-importer' ),
        webpackMainConfig   = require( './conf/webpack.config.js' ),
        webpackMerge        = require( 'webpack-merge' ),
        webpackStream       = require( 'webpack-stream' ),
        yargs               = require( 'yargs' )

/*
 | Gulp setup
 | ==========
 */
const   buildDir    = gulpConfig.paths.dist,
        config      = gulpConfig.config || {},
        output      = {},
        project     = gulpConfig.project,
        task        = yargs.argv._[ 0 ]

// Setup path outputs
if( config.platform === 'wordpress' ) {
    if( config.type === 'plugin' ) {
        output.files = buildDir + 'wp-content/plugins/' + config.name + '/';
    } else if( config.type === 'theme' ) {
        output.files = buildDir + 'wp-content/themes/' + config.name + '/';
    }	
} else {
	output.files = buildDir + '/';
}

output.build = buildDir + '/';

/**
 * -------------------------------------------------------------------------------------------------
 * Gulp Functions
 * -------------------------------------------------------------------------------------------------
 */

function browserServer( done ) {
    const browserSyncOptions = {
        logFileChanges: false,
        proxy:          config.proxy,
        xip:            true
    }
    
    if( typeof yargs.argv.nosync === 'undefined' ) {
        browserSync( browserSyncOptions )
    }

    done()
}

/*
 | CleanDist
 | =========
 | Deletes the build output directory
 */
const clean = () => {
    return del([
        output.files
    ])
}

/*
 | Conf
 | ====
 | Syncs any required config files
 */
const conf = () => {
    return gulp.src( project.conf.src )
        .pipe( changed( output.files + project.conf.dist ) )
        .pipe( gulp.dest( output.files + project.conf.dist ) )
        .pipe( browserSync.reload({ stream: true }) )
}

/*
 | Styles
 | ======
 | Compiles all stylesheets from SASS partials
 */
const styles = () => {
    const sassOptions = {
        errLogToConsole: true,
        importer: sassJsonImporter(),
        outputStyle: task === 'watch' ? 'compact' : 'compressed'        
    }

    return gulp.src( project.styles.src )
        .pipe( changed( output.files + project.styles.dist ) )
        .pipe( sass( sassOptions ).on( 'error', sass.logError ) )
        .pipe( gulp.dest( output.files + project.styles.dist ) )
        .pipe( browserSync.reload({ stream: true }) )
}

/*
 | Fonts
 | =====
 | Syncs all font files
 */
const fonts = () => {
	return gulp.src( project.fonts.src )
		.pipe( changed( output.files + project.fonts.dist ) )
        .pipe( gulp.dest( output.files + project.fonts.dist ) )
        .pipe( browserSync.reload({ stream: true }) )
}

/*
 | Htdocs
 | ======
 | Syncs all HTML, PHP and other template files
 */
const htdocs = () => {
    return gulp.src( project.htdocs.src )
        .pipe( changed( output.files + project.htdocs.dist ) )
        .pipe( gulpTap( ( file ) => {
            if( file.path.substr( -3 ) === 'php' ) {
                return gulp.src( file.path )
                    .pipe( phpLint( '', {
                        skipPassedFiles: true
                    }) )
                    .pipe( phpLint.reporter( ( file ) => {
                        let report = file.phplintReport || {};
                        
                        // Abort the build process if we get a PHP error
                        if( report.error && task !== 'watch' ) {
                            process.exit( 1 )
                        }
                    }) )                
            }
        }))
        .pipe( gulp.dest( output.files + project.htdocs.dist ) )
        .pipe( browserSync.reload({ stream: true }) )
}

/*
 | Images
 | ======
 | Compresses all image files and then syncs them
 */
const images = () => {
	return gulp.src( project.images.src )
		.pipe( changed( output.files + project.images.dist ) )
		.pipe( imagemin({
			optimizationLevel:	3,
			progressive:		true,
			interlaced:			true,
			svgoPlugins:		[{ removeViewBox: false }]
		}) )
		.pipe( gulp.dest( output.files + project.images.dist ) )
}

/*
 | Watcher
 | =======
 | Watch task for development environment
 */
const watcher = () => {
    gulp.watch( project.blocks.watch, sequenceBlocks )
    gulp.watch( project.conf.watch, sequenceConf )
    gulp.watch( project.fonts.watch, fonts )
    gulp.watch( project.htdocs.watch, htdocs )
    gulp.watch( project.images.watch, images )
    gulp.watch( project.scripts.watch, webpackScripts )
    gulp.watch( project.styles.watch, { emit: 'all' }, styles )
}

/*
 | Webpack - Components
 | ====================
 | Handles all JS components 
 */
const webpackComponents = () => {
    const webpackOptions = {
        entry:  [ '@babel/polyfill', path.resolve( __dirname, './src/assets/components/Index.jsx' ) ],
        mode:   task === 'watch' ? 'development' : 'production',
        output: {
            filename: 'app.js'
        }
    }

    return webpackStream( webpackMerge( webpackMainConfig, webpackOptions ) )
        .pipe( gulp.dest( output.files + '/js/' ) )
        .pipe( browserSync.reload({ stream: true }) )     
}

/*
 | Webpack - Scripts
 | =================
 | Handles all JS script compilation
 */
const webpackScripts = () => {
    return gulp.src( project.scripts.src )
        .pipe( gulpTap( ( file ) => {
            const webpackOptions = {
                entry:  path.resolve( file.path ),
                mode:   task === 'watch' ? 'development' : 'production',
                output: {
                    filename: path.basename( file.path )
                }
            }

            return webpackStream( webpackMerge( webpackMainConfig, webpackOptions ) )
                .pipe( gulp.dest( output.files + project.scripts.dist ) )
                .pipe( browserSync.reload({ stream: true }) )  
        }))
}

/**
 * -------------------------------------------------------------------------------------------------
 * Gulp Sequences
 * -------------------------------------------------------------------------------------------------
 */

// Sequence to execute when block files are changed
const sequenceBlocks = gulp.series(
    conf,
    htdocs,
    webpackScripts
)

// Sequence to run when building the project (i.e. making it production-ready)
const sequenceBuild = gulp.series(
    clean,
    conf,
    fonts,
    htdocs,
    images,
    styles,
    webpackScripts
)

// Sequence to execute when conf files are changed
const sequenceConf = gulp.series(
    conf,
    htdocs,
    styles
)

// Sequence to follow watch is active
const sequenceWatch = gulp.parallel( watcher, browserServer );

/**
 * -------------------------------------------------------------------------------------------------
 * Gulp Tasks
 * -------------------------------------------------------------------------------------------------
 */

// Expose the following tasks to the command line
exports.blocks  = sequenceBlocks
exports.build   = sequenceBuild
exports.conf    = sequenceConf
exports.fonts   = fonts
exports.htdocs  = htdocs
exports.images  = images
exports.styles  = styles
exports.watch   = sequenceWatch