<?php
/*
Plugin Name: Velocity SiteBuilder
Description: This plugin provides the ability to add pages to Wordpress from a Google Sheets doc.
Version: 1.0
Author: Velocity Partners
Author URI: http://www.velocitypartners.co.uk/
*/

namespace V_SITEBUILDER_PLUGIN;
session_start();

function v_plugin_init() {
    $void = new VelocitySitebuilder();
}

add_action('plugins_loaded', 'V_SITEBUILDER_PLUGIN\v_plugin_init', 10);

class VelocitySitebuilder {

    public function __construct() {
        global $wpdb;
        define(__NAMESPACE__ . '\V_SITEBUILDER_PLUGIN', 'v_SITEBUILDER_plugin');
        define(__NAMESPACE__ . '\V_PLUGIN_PATH', plugin_dir_path(__FILE__));
        define(__NAMESPACE__ . '\V_PLUGIN_URI', plugin_dir_url(__FILE__));

        // Loop through and fetch all of our includes
        $this->v_load_dependencies(namespace \V_PLUGIN_PATH . '/inc/');

        // Register our scripts and styles
        add_action('wp_enqueue_scripts', array($this, 'v_register_plugin_assets'));

        // create options page (need to add custom value)
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page();
        }

        // add markup to options page
        addMarkupToOptionsPage();

        // import posts to db 
        if (isset($_POST['addPostsToDb'])) {
            add_posts_to_db($wpdb);
        }
        
        // import media alt tags 
        if (isset($_POST['addAltTagsToMedia'])) {
            add_alt_tags_to_db($wpdb);
        }

        // export media to sheet 
        if (isset($_POST['exportMedia'])) {
            export_media_to_sheet($wpdb);
        }

        // export values from db to use in sheet 
        if (isset($_POST['exportValues'])) {
            export_taxonomy_to_sheet($wpdb, 'category', 'G2:G15');
            export_taxonomy_to_sheet($wpdb, 'tax_label', 'A2:A15');
            export_reusable_blocks($wpdb);
        }

        // return figma json in usable format 
        if (isset($_POST['getFigmaData'])) {
            get_data_from_figma();
            filter_raw_figma_data();
        }

        // export figma data to sheet 
        if (isset($_POST['FigmaToSheet'])) {
            $data = format_figma_data_to_acf_blocks();
            add_figma_data_to_sheets( $data );
        }
    }

    public function v_load_dependencies($file_path) {
        if (is_dir($file_path)) {
            $files = glob($file_path . '*', GLOB_MARK);

            foreach ($files as $file_path) {
                $this->v_load_dependencies($file_path);
            }
        } else {
            $file = pathinfo($file_path);

            if ($file['extension'] === 'php') {
                include_once $file_path;
            }
        }
    }

    public function v_register_plugin_assets() {
        // Core styles
        wp_enqueue_style( 'v-plugin-sitebuilder-css', namespace\V_PLUGIN_URI . 'css/v-sitebuilder.css', array(), filemtime( namespace\V_PLUGIN_PATH . 'css/v-sitebuilder.css' ), 'all' );
    }
}
