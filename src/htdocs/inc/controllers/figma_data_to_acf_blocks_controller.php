<?php

namespace V_SITEBUILDER_PLUGIN;


// formats data from Figma to return each page and modules as acf blocks
function format_figma_data_to_acf_blocks() {

    // get Figma json
    $file = __DIR__ . '/../figma_data.json';
    $figma_data = json_decode(file_get_contents($file), true);

    // initialise array to catch results to add to sheet
    $results_array = array();

    $count = 0;

    // loop through layers of Figma data 
    foreach ($figma_data as $key => $page) {

        $results_array[$count] = array();
        array_push($results_array[$count], $key);

        foreach ($page as $module_key => $module) {

            // set module names and content
            switch ($module_key) {
                case is_string(stristr($module_key, 'hero')):
                    $module_name = 'hero';
                    $content = get_hero_content($module_key, $module);

                    break;

                case is_string(stristr($module_key, 'media')) || is_string(stristr($module_key, 'icon'))   && is_string(stristr($module_key, '2columns')):
                    $module_name = 'wysiwyg-media';
                    $content = get_wysiwyg_media_content($module_key, $module);
                    break;


                case is_string(stristr($module_key, '1column')):
                    $module_name = 'wysiwyg-col-1';
                    $content = get_wysiwyg_1_col_content($module_key, $module);
                    break;

                case is_string(stristr($module_key, '2columns')):
                    $module_name = 'wysiwyg-col-2';
                    $content = get_wysiwyg_2_col_content($module_key, $module);
                    break;

                case is_string(stristr($module_key, '3columns')):
                    $module_name = 'wysiwyg-col-3';
                    break;

                case is_string(stristr($module_key, 'column')) && is_string(stristr($module_key, '4')):
                    $module_name = 'wysiwyg-col-4';
                    break;

                case is_string(stristr($module_key, 'feed')):
                    $module_name = 'feed';
                    break;

                case is_string(stristr($module_key, 'cta')):
                    $module_name = 'cta';
                    break;
            }

            // add each page with modules to results array to be added to sheet
            $module_content = output_module_as_acf_block($module_name, $content);
            array_push($results_array[$count], $module_content);
        }
        $count += 1;
    }

    return $results_array;
}
