<?php

namespace V_SITEBUILDER_PLUGIN;

// take raw data and pull what we need from it
// then saves to new file
function filter_raw_figma_data() {

    // set file to receive JSON from
    $file = __DIR__ . '/../figma_data.json';

    // decode JSON as array
    $result = json_decode(file_get_contents($file), true);

    // get layout section of Figma file
    foreach ((array) $result['document']['children'] as $sections) {
        if ('Layout' === $sections['name']) {
            $layout = $sections['children'];
        }
    }

    // initialise array to store filtered data
    $results_array = array();

    // loop through each level of the array to return a ist of pages with modules and content
    foreach ((array) $layout as $page) {
        if (!stristr($page['name'], 'Mobile')) {
            foreach ((array) $page['children'] as $module) {

                // removes modules with nav in title, e.g. header, breadcrumbs, footer
                if (!stristr($module['name'], 'nav')) {

                    // get rules to pull data from different modules
                    switch ($module['name']) {

                        // get content for benefits module
                        case is_string(stristr($module['name'], 'benefits')):
                            foreach ($module['children'] as $module_content) {
                                switch ($module_content['name']) {

                                    // get new module & copy for wysiwyg 1 column above benefits module
                                    case 'copy':
                                        foreach ($module_content['children'] as $copy) {
                                            $subtitle_count = 0;
                                            foreach ($copy['children'] as $copy_type) {
                                                if ('subtitle' === $copy_type['name']) {
                                                    $results_array[$page['name']]['wysiwyg / 1column / editor: medium']['content'][$copy['name']][$copy_type['name'] . ' - ' . $subtitle_count] = $copy_type['characters'];
                                                    $subtitle_count += 1;
                                                } else {
                                                    $results_array[$page['name']]['wysiwyg / 1column / editor: medium']['content'][$copy['name']][$copy_type['name']] = $copy_type['characters'];
                                                }
                                            }
                                        }
                                        break;

                                    // get image and copy for each benefits column
                                    case 'benefits':
                                        foreach ($module_content['children'] as $columns) {
                                            foreach ($columns['children'] as $column_content) {
                                                switch ($column_content['name']) {
                                                    case 'graphic':

                                                        // do stuff for image of benefit column

                                                        break;

                                                    // get copy for each column
                                                    case 'copy':
                                                        $count = 0;
                                                        foreach ($column_content['children'] as $copy_type) {
                                                            $results_array[$page['name']][$module['name']][$module_content['name']][$columns['name']][$column_content['name']][$copy_type['name'] . ' - ' . $count] = $copy_type['characters'];
                                                            $count += 1;
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                        break;
                                }
                            }
                            break;

                        // get content for wysiwyg 2 columns
                        case is_string(stristr($module['name'], '2columns / editor +')) && !is_string(stristr($module['name'], 'media')):
                            foreach ($module['children'] as $module_content) {
                                foreach ($module_content['children'] as $columns) {
                                    foreach ($columns['children'] as $column_content) {

                                        // sort copy and button content
                                        switch ($column_content['name']) {
                                            case 'copy':
                                                foreach ((array) $column_content['children'] as $copy_type) {
                                                    $results_array[$page['name']][$module['name']][$module_content['name']][$columns['name']][$column_content['name']][$copy_type['name']] = $copy_type['characters'];
                                                }
                                                break;

                                            case 'buttons':
                                                $count = 0;
                                                foreach ($column_content['children'] as $button) {

                                                    // checks if button is hidden
                                                    if (false !== $button['visible']) {
                                                        if ('text' !== $button['children'][1]['name']) {
                                                            $results_array[$page['name']][$module['name']][$module_content['name']][$columns['name']][$column_content['name']][$button['name'] . ' - ' . $count]['text'] = $button['children'][1]['characters'];
                                                        } else {
                                                            $results_array[$page['name']][$module['name']][$module_content['name']][$columns['name']][$column_content['name']][$button['name'] . ' - ' . $count]['text'] = $button['children'][1]['children'][0]['characters'];
                                                        }
                                                        $count += 1;
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                }
                            }
                            break;


                        // get content for feed module
                        case is_string(stristr($module['name'], 'feed')):

                            // add feed module content

                            break;

                        default:
                            foreach ((array) $module['children'] as $column_content) {
                                // $results_array[$page['name']][$module['name']][$column_content['name']] = array();

                                // switch satement to treat copy/buttons differently to images
                                switch ($column_content['name']) {

                                    case is_string(stristr($column_content['name'], 'content')):

                                        foreach ($column_content['children'] as $content) {

                                            // if statement to pull correct information for some modules,
                                            // where content is 1 level deeper in the array
                                            if ('content left' === $content['name']) {
                                                foreach ($content['children'] as $content_left) {
                                                    // $results_array[$page['name']][$module['name']][$column_content['name']][$content['name']][$content_left['name']] = array();

                                                    // get copy for module, usually as title, subtitle and body
                                                    if ('copy' === $content_left['name']) {
                                                        foreach ($content_left['children'] as $content_left_copy) {
                                                            $results_array[$page['name']][$module['name']][$column_content['name']][$content_left['name']][$content_left_copy['name']] = $content_left_copy['characters'];
                                                        }
                                                    }

                                                    // get buttons for module with unique identifier if there is more than one
                                                    if ('buttons' === $content_left['name'] && false !== $content_left['visible']) {
                                                        $count = 0;
                                                        foreach ($content_left['children'] as $buttons) {

                                                            // checks if button is hidden
                                                            if (false !== $buttons['visible']) {
                                                                // if statment to pull correct text when some text is nested 1 level deeper
                                                                if (' text' !== $buttons['children'][1]['name']) {
                                                                    $results_array[$page['name']][$module['name']][$column_content['name']][$content_left['name']][$buttons['name'] . ' - ' . $count]['text'] = $buttons['children'][1]['characters'];
                                                                } else {
                                                                    $results_array[$page['name']][$module['name']][$column_content['name']][$content_left['name']][$buttons['name'] . ' - ' . $count]['text'] = $buttons['children'][1]['children'][0]['characters'];
                                                                }
                                                                $count += 1;
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {

                                                // get copy for module, usually as title, subtitle and body
                                                if ('copy' === $content['name']) {
                                                    foreach ($content['children'] as $regular_content_copy) {
                                                        $results_array[$page['name']][$module['name']]['content'][$content['name']][$regular_content_copy['name']] = $regular_content_copy['characters'];
                                                    }
                                                }

                                                // checks if button is hidden
                                                if (false !== $content['visible']) {
                                                    // get buttons for module with unique identifier if there is more than one
                                                    if ('buttons' === $content['name']) {
                                                        $count = 0;
                                                        foreach ($content['children'] as $button) {
                                                            if (false !== $button['visible']) {
                                                                if ('text' !== $button['children'][1]['name']) {
                                                                    $results_array[$page['name']][$module['name']]['content'][$content['name']][$button['name'] . ' - ' . $count]['text'] = $button['children'][1]['characters'];
                                                                } else {
                                                                    $results_array[$page['name']][$module['name']]['content'][$content['name']][$button['name'] . ' - ' . $count]['text'] = $button['children'][1]['children'][0]['characters'];
                                                                }
                                                                $count += 1;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        break;

                                    // check if content type is an image
                                    case is_string(stristr($column_content['name'], 'image')):

                                        // will do stuff to pull image info from json

                                        break;
                                }
                            }
                    }
                }
            }
        }
    }

    // saves filtered results to file for later use
    file_put_contents(__DIR__ . '/../figma_data.json', json_encode($results_array));
}