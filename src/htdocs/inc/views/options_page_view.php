<?php

namespace V_SITEBUILDER_PLUGIN;

// add markup to settings page
function addMarkupToOptionsPage() {
        
    // checks for acf options page (needs changed to use custom page name, to only appear on this page)
    if (strpos(home_url($_SERVER['REQUEST_URI']), 'acf-options')) {

        // set form with button to activate addToDb function
        ?>
        <div style='position:absolute; right: 30px; bottom: 50px; z-index:100;'>
            <form  method="post" action=" <?php echo home_url($_SERVER['REQUEST_URI']) ?>">
                <button style='padding: 10px; background:tomato; font-size: 20px; color:black; border-radius:10px; z-index:100; cursor: pointer;'
                type="submit" name="addPostsToDb" >Import pages and posts</button>
            </form>
            
            <form method="post" action=" <?php echo home_url($_SERVER['REQUEST_URI']) ?>">
                <button style='padding: 10px; background:tomato; font-size: 20px; color:black; border-radius:10px; z-index:100; cursor: pointer;'
                type="submit" name="addAltTagsToMedia" >Import media alt tags </button>
            </form>

            <form method="post" action=" <?php echo home_url($_SERVER['REQUEST_URI']) ?>">
                <button style='padding: 10px; background:paleGreen; font-size: 20px; color:black; border-radius:10px; z-index:100; cursor: pointer;'
                type="submit" name="exportMedia" >Export media library to sheet</button>
            </form>

            <form method="post" action=" <?php echo home_url($_SERVER['REQUEST_URI']) ?>">
                <button style='padding: 10px; background:paleGreen; font-size: 20px; color:black; border-radius:10px; z-index:100; cursor: pointer;'
                type="submit" name="exportValues" >Export values to sheet</button>
            </form>

            <form method="post" action=" <?php echo home_url($_SERVER['REQUEST_URI']) ?>">
                <button style='padding: 10px; background:darkOrchid; font-size: 20px; color:black; border-radius:10px; z-index:100; cursor: pointer;'
                type="submit" name="getFigmaData" >Get Figma Data</button>
            </form>

            <form method="post" action=" <?php echo home_url($_SERVER['REQUEST_URI']) ?>">
                <button style='padding: 10px; background:darkOrchid; font-size: 20px; color:black; border-radius:10px; z-index:100; cursor: pointer;'
                type="submit" name="FigmaToSheet" >Add Figma Data to Sheet</button>
            </form>

        </div>

        <?php
    }
}