<?php

namespace V_SITEBUILDER_PLUGIN;

// return content for wysiwyg Media module
function get_wysiwyg_media_content($module_key, $module) {
    // set order 
    switch ($module_key) {
        case is_string(stristr($module_key, 'left')):
            $desktop_order = 'media_content';
            break;

        default:
            $desktop_order = 'content_media';
            break;
    }

    // get content as a string
    $content = get_copy_and_buttons($module_key, $module);

    // set data unique to module
    $data = '
        "data": {
            "content": "' . $content . '",
            "_content": "wysiwyg-media_content",
            "media": "image",
            "_media": "wysiwyg-media_media",
            "image": "",
            "_image": "wysiwyg-media_image",
            "order_mobile": "media_content",
            "_order_mobile": "wysiwyg-media_order_mobile",
            "order_desktop": "' . $desktop_order . '",
            "_order_desktop": "wysiwyg-media_order_desktop"
        },
    ';

    return $data;
}