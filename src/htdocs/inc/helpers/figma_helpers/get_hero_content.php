<?php

namespace V_SITEBUILDER_PLUGIN;

// return content for hero module
function get_hero_content($module_key, $module) {

    // set layout
    switch ($module_key) {
        case is_string(stristr($module_key, 'secondary')):
            $layout = 'secondary';
            break;

        case is_string(stristr($module_key, 'tertiary')):
            $layout = 'tertiary';
            break;

        default:
            $layout = 'primary';
            break;
    }

    // get content as a string
    $content = get_copy_and_buttons($module_key, $module);

    // set data to module
    $data = '
        "data": {
        "content_source": "custom",
        "_content_source": "hero_content_source",
        "content": "' . $content . '",
        "_content": "hero_content",
        "media": "image",
        "_media": "hero_media",
        "featured_image": "",
        "_featured_image": "hero_featured_image",
        "image": "",
        "_image": "hero_image",
        "layout": "' . $layout . '",
        "_layout": "hero_layout"},
    ';

    return $data;
}