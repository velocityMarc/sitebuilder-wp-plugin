<?php

namespace V_SITEBUILDER_PLUGIN;

// return content for wysiwyg 2-col module
function get_wysiwyg_2_col_content($module_key, $module) {
    // set width 
    switch ($module_key) {
        case is_string(stristr($module_key, 'small')):
            $width = 6;
            break;

        case is_string(stristr($module_key, 'large')):
            $width = 10;
            break;

        default:
            $width = 8;
            break;
    }

    // get content as a string
    $content = get_copy_and_buttons($module_key, $module);

    // set data unique to module
    $data = '
    "data": {
        "content_1": "' . $content[0] . '",
        "_content_1": "wysiwyg-col-2_content_1",
        "content_2": "' . $content[1] . '",
        "_content_2": "wysiwyg-col-2_content_2",
        "layout": "inline",
        "_layout": "wysiwyg-col-2_layout",
        "align_v": "top",
        "_align_v": "wysiwyg-col-2_align_v",
        "order_mobile": "content_1_content_2",
        "_order_mobile": "wysiwyg-col-2_order_mobile",
        "order_desktop": "content_1_content_2",
        "_order_desktop": "wysiwyg-col-2_order_desktop"
    },

    ';

    return $data;
}