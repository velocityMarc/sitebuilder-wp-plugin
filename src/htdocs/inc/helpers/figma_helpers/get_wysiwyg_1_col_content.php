<?php

namespace V_SITEBUILDER_PLUGIN;

// return content for wysiwyg 1-col module
function get_wysiwyg_1_col_content($module_key, $module) {
    // set width 
    switch ($module_key) {
        case is_string(stristr($module_key, 'small')):
            $width = 6;
            break;

        case is_string(stristr($module_key, 'large')):
            $width = 10;
            break;

        default:
            $width = 8;
            break;
    }

    // get content as a string
    $content = get_copy_and_buttons($module_key, $module);

    // set data unique to module
    $data = '
        "data": {
            "content": "' . $content . '",
            "_content": "wysiwyg-col-1_content",
            "width_content": ' . $width . ',
            "_width_content": "wysiwyg-col-1_width_content",
            "align_h": "left",
            "_align_h": "wysiwyg-col-1_align_h",
            "layout": "normal",
            "_layout": "wysiwyg-col-1_layout"
        },
    ';

    return $data;
}