<?php

namespace V_SITEBUILDER_PLUGIN;

// return string with copy and button data
function get_copy_and_buttons($module_key, $module) {

    // check if 2 col wysiwyg
    if (is_string(stristr($module_key, '2columns / editor +')) && !is_string(stristr($module_key, 'media'))) {
        $content = array();

        $count = 0;
        foreach ($module['content'] as $columns) {
            foreach ((array) $columns['copy'] as $copy) {
                $content[$count] .= $copy . '\r\n\r\n';
            }

            // add module buttons
            foreach ((array) $columns['buttons'] as $button_key => $button) {
                if (stristr($button_key, 'primary')) {
                    $this_button = addslashes('<a class="v-btn v-btn-primary" href="">' . $button['text'] . '</a>');
                }

                if (stristr($button_key, 'secondary')) {
                    $this_button = addslashes('<a class="v-btn v-btn-secondary" href="">' . $button['text'] . '</a>');
                }
                $content[$count] .= $this_button . '\r\n\r\n';
            }

            $count += 1;
        }
    } else {
        // initialise variable to catch content string
        $content = '';

        // add module copy
        foreach ((array) $module['content']['copy'] as $copy) {
            $content .= $copy . '\r\n\r\n';
        }

        // add module buttons
        foreach ((array) $module['content']['buttons'] as $button_key => $button) {
            if (stristr($button_key, 'primary')) {
                $this_button = addslashes('<a class="v-btn v-btn-primary" href="">' . $button['text'] . '</a>');
            }

            if (stristr($button_key, 'secondary')) {
                $this_button = addslashes('<a class="v-btn v-btn-secondary" href="">' . $button['text'] . '</a>');
            }
            $content .= $this_button . '\r\n\r\n';
        }
    }

    return $content;
}