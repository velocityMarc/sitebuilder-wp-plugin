<?php

namespace V_SITEBUILDER_PLUGIN;

// function to return string for a module with unique id wrapped in a container
function output_module_as_acf_block($module_name, $data) {
    $container_start = '<!-- wp:v-wp-theme/v-block-container --> ';
    $container_end = ' <!-- /wp:v-wp-theme/v-block-container --> ';
    $module_start =   '<!-- wp:acf/' . $module_name . ' { "id": "block_' . uniqid() . '","name": "acf\/' . $module_name . '",';
    $module_end = ' "mode": "preview" } /-->';

    return $container_start . $module_start . $data . $module_end . $container_end;
}