<?php

namespace V_SITEBUILDER_PLUGIN;

// gets meta id from post id
function get_meta_id($wpdb, $post_id, $meta_key) {
    $label_table = $wpdb->prefix . 'postmeta';
    $statement = "SELECT meta_id FROM " . $label_table . " WHERE post_id='" . $post_id . "' AND  meta_key='" . $meta_key . "'";
    return $wpdb->get_var($statement);
}