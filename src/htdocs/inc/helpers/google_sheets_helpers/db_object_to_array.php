<?php

namespace V_SITEBUILDER_PLUGIN;

// convert results from db to an array
function db_object_to_array($db_object) {
    $sheet_values = array();

    foreach($db_object as $result) {
        $result = json_decode(json_encode($result), true);
        array_push($sheet_values, array_values($result));
    }

    return $sheet_values;
}