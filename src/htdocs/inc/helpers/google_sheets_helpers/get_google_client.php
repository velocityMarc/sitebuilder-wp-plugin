<?php

require_once __DIR__ . '/../../../../../../vendor/autoload.php';

// sets params for Google API CLient
function get_google_client() {

    $client = new Google_Client();
    $client->setApplicationName('Google Sheets API PHP Quickstart');
    $client->setScopes(Google_Service_Sheets::SPREADSHEETS);
    $client->setAuthConfig(__DIR__ . '/../../../credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');
    $client = new Google_Service_Sheets($client);

    return $client;
}

function get_serivce_sheets_value_range($range, $values, $major_dimensions = 'ROWS') {
    return new Google_Service_Sheets_ValueRange([
        'range' => $range,
        'majorDimension' => $major_dimensions,
        'values' => $values
    ]);
}