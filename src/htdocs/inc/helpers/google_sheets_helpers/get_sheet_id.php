<?php

namespace V_SITEBUILDER_PLUGIN;

// returns sheet id from Google sheets url
function get_sheet_id() {

    if(get_option('options_sheets_url')) {
        //get full url of Google Sheet to use
        $sheet_url = get_option('options_sheets_url');
        
        // regex to extract sheets id from url
        $exp = "/\/spreadsheets\/d\/([a-zA-Z0-9-_]+)/";
        preg_match($exp, $sheet_url, $matches);

        return $matches[1];
    } else {
        //use test sheet by default
        return '1LUU-GDl29emdcwmoxs8TjVPFVns6AsVERbkxGn8eFA4';
    }
}