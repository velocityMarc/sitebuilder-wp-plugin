<?php

namespace V_SITEBUILDER_PLUGIN;

// get raw data from Figma as JSON
// saves to file due to large size
function get_data_from_figma() {

    // set params for curl request
    $base_url = 'https://api.figma.com/v1/files/';

    // get figma url from options page in wordpress
    if (get_option('options_figma_project_url')) {
        $figma_url = get_option('options_figma_project_url');

        // regex to extract figma id from url
        $exp = "/\/file\/([a-zA-Z0-9-_]+)/";
        preg_match($exp, $figma_url, $matches);

        $file_id = $matches[1];
    } else {
        $file_id = 'CZFNPalX2vcKJL7Ndk5s3x';
    }

    // add params to base url
    $url = $base_url . $file_id;

    // Initialize a CURL session. 
    $ch = curl_init();

    // Return Page contents. 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // add access token to Figma
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'X-Figma-Token: 58164-74a2837b-cf71-464b-9610-ea7803666f09'
    ));

    //grab URL and pass it to the variable. 
    curl_setopt($ch, CURLOPT_URL, $url);

    // get result from curl request
    $result = curl_exec($ch);

    // set file to store results
    $file = __DIR__ . '/../figma_data.json';

    // store results in file due to large size
    file_put_contents($file, $result);
}