<?php

namespace V_SITEBUILDER_PLUGIN;

// returns db object with postmeta data
function get_media_from_db($wpdb, $columns, $meta_key) {
    // set table name
    $postmeta_table = $wpdb->prefix . 'postmeta';

    // get media library from db
    $results = $wpdb->get_results( 
        $wpdb->prepare("SELECT " . $columns . " FROM $postmeta_table WHERE meta_key='" . $meta_key . "';")
    );

    return $results;
}

// upload alt tags for media to wpdb
function add_alt_tags_to_db($wpdb) {
    
    // get sheet values
    $range = 'content_export!A3:E200';
    $sheet_values = get_post_data_from_sheet($range);
    
    // nested foreach loop to select value of each sheet cell
    foreach ( (array) $sheet_values as $row) {
    
        // set variables as descriptions for sheet columns
        $id       = $row[0];
        $alt_id   = $row[3];
        $alt_tag  = $row[4];
    
        foreach ($row as $value) {
            switch ($value) {
    
                // set post id
                case $id:
                    $post_id = $value;
                    continue;
    
                // set alt meta id
                case $alt_id:
                    $alt_meta_id = $value;
                    continue;
    
                // set meta key and value
                case $alt_tag:
                    $meta_key = '_wp_attachment_image_alt';
                    $meta_value = $value;
    
                break;
    
                }   
            }
    
        // set params for wpdb replace
        $postmeta_table = $wpdb->prefix . 'postmeta';
        $postmeta_data = array(
            'post_id'    => $post_id,
            'meta_id'    => $alt_meta_id,
            'meta_key'   => $meta_key,
            'meta_value' => $meta_value
        );
        
        // insert/replace into db 
        $wpdb->replace($postmeta_table, $postmeta_data); 
    
    }
    
}


// inserts or replaces post data in db
function replace_posts($wpdb, $post_id, $post_content, $post_title, $post_excerpt, $post_slug, $post_type, $post_status, $comment_status, $post_date, $post_parent){
    
    // set post table
    $posts_table = $wpdb->prefix . 'posts';
    
    // set data to input into db, using column name as the key
    $post_data = array(
        'id'             => $post_id,
        'post_author'    => '1',
        'post_content'   => $post_content,
        'post_title'     => $post_title,
        'post_excerpt'   => $post_excerpt,
        'post_name'      => $post_slug,
        'post_type'      => $post_type,
        'post_status'    => $post_status,
        'comment_status' => $comment_status,
        'post_date'      => $post_date,
        'post_parent'    => $post_parent
    );
    
    // insert/replace into db 
    $wpdb->replace($posts_table, $post_data);
}

// insert/replace postmeta data in db
function replace_post_meta($wpdb, $post_id, $cta_label, $yeost_keyword, $yeost_meta_desc) {
    
    // set postmeta table
    $postmeta_table = $wpdb->prefix . 'postmeta';
    // set keys to insert meta values
    $meta_keys = array('cta_label', '_yoast_wpseo_focuskw', '_yoast_wpseo_metadesc');
    
    foreach ($meta_keys as $key) {
        switch ($key){
            case 'cta_label' :
                $meta_value = $cta_label;
            break;
            
            case '_yoast_wpseo_focuskw' :
                $meta_value = $yeost_keyword;
            break;
            
            case '_yoast_wpseo_metadesc' :
                $meta_value = $yeost_meta_desc;
            break;
        }

        $meta_id = get_meta_id($wpdb, $post_id, $key);
        
        // set data to input into postmeta table
        $postmeta_data = array(
            'meta_id' => $meta_id,
            'post_id' => $post_id,
            'meta_key' => $key,
            'meta_value' => $meta_value
        );
        
        // insert or update row in postmeta table
        $wpdb->replace($postmeta_table, $postmeta_data);
    }
}

// insert/replace post category
function replace_post_category($wpdb, $post_id, $tax_id) {
    
    $table = $wpdb->prefix . 'term_relationships';
    $term_data = array(
        'object_id' => $post_id,
        'term_taxonomy_id' => $tax_id
    );

    // insert or update row in postmeta table
    $wpdb->replace($table, $term_data);
}

/* 
Insert/replace pages into Wordpress db from Google Sheet values.
$wpdb Object 
$sheet_values Array of values from Google Sheet
*/
function add_posts_to_db($wpdb) {

    $range = 'posts!B3:ZZ200';
    $sheet_values = get_post_data_from_sheet($range);

    // nested foreach loop to select value of each sheet cell
    foreach ($sheet_values as $row) {

        // set variables as descriptions for sheet columns
        $id          = $row[0];
        $title       = $row[1];
        $date        = $row[2];
        $slug        = $row[3];
        $keyword     = $row[4];
        $meta_desc   = $row[5];
        $excerpt     = $row[6];
        $cta_label   = $row[7];
        $type        = $row[8];
        $status      = $row[9];
        $com_status  = $row[10];
        $post_cat    = $row[11];
        $parent_post = $row[12];


        foreach ($row as $value) {

            // determine the content of each cell.
            switch ($value) {

                // set post id
                case $id:
                    if('' === $value) {
                        break;
                    } else {
                        $post_id = $value;
                        continue;
                    }

                // set post title
                case $title:
                    if('' === $value) {
                        break;
                    } else {
                        $post_title = $value;
                        continue;
                    }

                // set post slug
                case $date:
                    $post_date = $value;
                    continue;

                // set post slug
                case $slug:
                    $post_slug = $value;
                    continue;

                // set yeaost keyword
                case $keyword:
                    $yeost_keyword = $value;
                    continue;

                // set yeost meta description
                case $meta_desc:
                    $yeost_meta_desc = $value;
                    continue;

                // set post excerpt
                case $excerpt:
                        $post_excerpt = $value;
                    continue;

                // set post cta label
                case $cta_label:
                    $post_cta_label = get_tax_id($wpdb, $value);
                    continue;

                // set post type
                case $type:
                    $post_type = $value;
                    continue;

                // set post status
                case $status:
                    $post_status = $value;
                    continue;

                // set comment status
                case $com_status:
                    $comment_status = $value;
                    continue;

                // set post category 
                case $post_cat:
                    $tax_id = get_tax_id($wpdb, $value);
                    continue;

                // set post parent 
                case $parent_post:
                    $post_parent = $value;
                    continue;

                // set module content
                case is_string(stristr($value, 'v-block-container')) :
                    $post_content = $value;

                    break;
            }
        }

        // insert/replace data in postmeta table
        replace_posts($wpdb, $post_id, $post_content, $post_title, $post_excerpt, $post_slug, $post_type, $post_status, $comment_status, $post_date, $post_parent);
        
        // insert/replace data in postmeta table
        replace_post_meta($wpdb, $post_id, $post_cta_label, $yeost_keyword, $yeost_meta_desc);

        // insert/replace post category in term relationship table
        if('post' === $post_type) {
            replace_post_category($wpdb, $post_id, $tax_id);
        }
    }

    // set post excerpt to auto, if none given
    $wpdb->query(
        $wpdb->prepare("UPDATE " . $wpdb->prefix . "posts SET post_excerpt ='' WHERE post_excerpt ='Sample Excerpt'"
        )
    );
}