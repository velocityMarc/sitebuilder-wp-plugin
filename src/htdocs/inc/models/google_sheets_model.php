<?php

namespace V_SITEBUILDER_PLUGIN;

require_once __DIR__ . '/../helpers/google_sheets_helpers/get_google_client.php';

// get posts from Google Sheet
function get_post_data_from_sheet($range) {

    $client = get_google_client();

    $sheet_id = get_sheet_id();

    // get values from sheet using sheetID and range
    $response = $client->spreadsheets_values->get($sheet_id, $range);
    $values = $response->getValues();
    
    return $values;
}

// add media to Google sheet
function export_media_to_sheet($wpdb) {

    // create new Google api client
    $client = get_google_client();

    // get id and range for sheet
    $sheet_id = get_sheet_id();

    // get values from db
    $media_path_values = get_media_from_db($wpdb, '`post_id`, `meta_id`, `meta_value`', '_wp_attached_file');
    $alt_tag_values = get_media_from_db($wpdb, '`meta_id`, `meta_value`', '_wp_attachment_image_alt');

    $media_path_sheet_values = db_object_to_array($media_path_values);
    $alt_tag_sheet_values = db_object_to_array($alt_tag_values);

    $media_path_range = 'content_export!A3:C2000';
    // set body for adding media paths to sheet
    $media_path_body = get_serivce_sheets_value_range($media_path_range, $media_path_sheet_values);

    $alt_tag_range = 'content_export!D3:E2000';
    // set body for adding alt tags to sheet
    $alt_tag_body = get_serivce_sheets_value_range($alt_tag_range, $alt_tag_sheet_values);

    // show values as if typed by a user. Removes additional characters
    $params = [
        'valueInputOption' => 'USER_ENTERED'
    ];

    // add media path values to sheet
    $client->spreadsheets_values->update($sheet_id, $media_path_range, $media_path_body, $params);

    // add alt tags to sheet
    $client->spreadsheets_values->update($sheet_id, $alt_tag_range, $alt_tag_body, $params);
}

// add taxonomy to Google sheet
function export_taxonomy_to_sheet($wpdb, $taxonomy, $range) {

    $prefix = $wpdb->prefix;
    $results = $wpdb->get_results(
        $wpdb->prepare(
            "SELECT " . $prefix . "terms.name
            FROM " . $prefix . "term_taxonomy AS t
            INNER JOIN " . $prefix . "terms ON t.term_id = " . $prefix . "terms.term_id
            AND t.taxonomy = '" . $taxonomy . "'"
        )
    );

    $values = db_object_to_array($results);

    // create new Google api client
    $client = get_google_client();

    // get id and range for sheet
    $sheet_id = get_sheet_id();
    $range = 'values!' . $range;
    // set body for adding media paths to sheet
    $body = get_serivce_sheets_value_range($range, $values);

    // show values as if typed by a user. Removes additional characters
    $params = [
        'valueInputOption' => 'USER_ENTERED'
    ];

    // add media path values to sheet
    $client->spreadsheets_values->update($sheet_id, $range, $body, $params);
}

// add reusable block to Google sheet
function export_reusable_blocks($wpdb) {

    $prefix = $wpdb->prefix;
    $result = $wpdb->get_results(
        $wpdb->prepare(
            "Select post_title
            FROM " . $prefix . "posts 
            WHERE post_type ='wp_block'"
            )
        );
        
    $client = get_google_client();
    $sheet_id = get_sheet_id();
    $results = db_object_to_array($result);
    $sheet_values = array();


    // prefix reusable blocks with 'RB-'
    foreach($results as $blocks) {
        foreach($blocks as $block) {
            $temp_array = array();
            $block = 'RB-' . $block;
            array_push($temp_array, $block);
        }
        array_push($sheet_values, $temp_array);
    }

    $range = 'values!H2:H200';
    $body = get_serivce_sheets_value_range($range, $sheet_values);

    $params = [
        'valueInputOption' => 'USER_ENTERED'
    ];

    // add reusable blocks to sheet
    $client->spreadsheets_values->update($sheet_id, $range, $body, $params);
}

// add pages & modules from Figma to sheet
function add_figma_data_to_sheets($data) {
        
    $client = get_google_client();
    $sheet_id = get_sheet_id();
    $sheet_values = $data;

    $range = 'figma_data!B2:AZ100';
    $body = get_serivce_sheets_value_range($range, $sheet_values);

    $params = [
        'valueInputOption' => 'USER_ENTERED'
    ];

    // add reusable blocks to sheet 
    $client->spreadsheets_values->update($sheet_id, $range, $body, $params);

}